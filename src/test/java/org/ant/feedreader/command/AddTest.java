package org.ant.feedreader.command;

import org.ant.feedreader.DummyFeedContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.junit.Assert;
import org.junit.Test;

public class AddTest {

  @Test
  public void shouldAddSimpleUrl() {
    String url = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public void add(Feed feed) {
            Assert.assertEquals("Feed should contain original url", url, feed.getUrl());
          }
        };

    Command command = new Add(feeds);
    command.execute("add " + url);
  }

  @Test
  public void shouldTrimUrl() {
    String url = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public void add(Feed feed) {
            Assert.assertEquals("Feed should contain trimmed url", url, feed.getUrl());
          }
        };

    Command command = new Add(feeds);
    command.execute("add " + "  " + url + "   ");
  }

  @Test
  public void shouldHaveDefaultTarget() {
    String url = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public void add(Feed feed) {
            Assert.assertEquals(
                "Feed should have default target on creation ", url, feed.getTarget());
          }
        };

    Command command = new Add(feeds);
    command.execute("add " + url);
  }
}
