package org.ant.feedreader.service;

import org.ant.feedreader.DummyFeedContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.ContentEntryFieldType;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.io.FileInput;
import org.ant.feedreader.io.FileOutput;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;

public class FeedPersistenceServiceTest {
  @Test
  public void shouldLoadFeedsFromJson() {
    String url = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public void add(Feed feed) {
            Assert.assertEquals(url, feed.getUrl());
            Assert.assertEquals(url, feed.getTarget());
            Assert.assertArrayEquals(
                "Should load all feed filters",
                new ContentEntryFieldType[] {
                  ContentEntryFieldType.TITLE,
                  ContentEntryFieldType.DESCRIPTION,
                  ContentEntryFieldType.SOURCE
                },
                feed.getFilters().toArray());
          }

          @Override
          public void addMany(Collection<Feed> feeds) {
            for (Feed feed : feeds) {
              add(feed);
            }
          }
        };

    FileInput input =
        new FileInput() {
          @Override
          public boolean fileExist(String path) {
            return true;
          }

          @Override
          public String load(String path) {
            return "[{\"url\":\"https://example.test/rss\",\"target\":\"https://example.test/rss\",\"countToOutput\":100,\"filters\":[\"TITLE\",\"DESCRIPTION\",\"SOURCE\"],\"enabled\":true,\"lastAccess\":{\"date\":{\"year\":2019,\"month\":1,\"day\":1},\"time\":{\"hour\":0,\"minute\":1,\"second\":1,\"nano\":0}},\"delay\":30,\"status\":\"loaded\"}]";
          }
        };

    FileOutput output = (path, input1) -> {};

    FeedPersistenceService feedLoader = new FeedPersistenceService(feeds, input, output);

    feedLoader.init();
    feedLoader.run();
    feedLoader.shutdown();
  }
}
