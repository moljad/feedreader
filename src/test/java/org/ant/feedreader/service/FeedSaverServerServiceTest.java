package org.ant.feedreader.service;

import org.ant.feedreader.DummyContentContainer;
import org.ant.feedreader.DummyFeedContainer;
import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.*;
import org.ant.feedreader.io.FileOutput;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class FeedSaverServerServiceTest {
  @Test
  public void shouldWriteToFile() {
    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public List<Feed> getAll() {
            Feed feed = new FeedBuilder("https://example.test/rss").build();

            return Collections.singletonList(feed);
          }
        };

    ContentContainer contents =
        new DummyContentContainer() {
          @Override
          public List<Content> getAll() {
            Map<ContentEntryFieldType, String> fields = new HashMap<>();
            fields.put(ContentEntryFieldType.TITLE, "title 1");
            fields.put(ContentEntryFieldType.DESCRIPTION, "description 1");

            ContentEntry entry = new ContentEntry(fields);

            Content content =
                new ContentBuilder("https://example.test/rss")
                    .withEntries(Collections.singleton(entry))
                    .build();

            return Collections.singletonList(content);
          }
        };

    FileOutput output =
        (path, text) -> Assert.assertEquals(text, "TITLE: title 1\nDESCRIPTION: description 1\n\n");

    FeedSaverService feedSaver = new FeedSaverService(feeds, contents, output);
    feedSaver.init();
    feedSaver.run();
    feedSaver.shutdown();
  }

  @Test
  public void shouldFilterOutput() {
    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public List<Feed> getAll() {
            Feed feed =
                new FeedBuilder("https://example.test/rss")
                    .withFilters(Collections.singleton(ContentEntryFieldType.TITLE))
                    .build();

            return Collections.singletonList(feed);
          }
        };

    ContentContainer contents =
        new DummyContentContainer() {
          @Override
          public List<Content> getAll() {
            Map<ContentEntryFieldType, String> fields = new HashMap<>();
            fields.put(ContentEntryFieldType.TITLE, "title 1");
            fields.put(ContentEntryFieldType.DESCRIPTION, "description 1");

            ContentEntry entry = new ContentEntry(fields);

            Content content =
                new ContentBuilder("https://example.test/rss")
                    .withEntries(Collections.singleton(entry))
                    .build();

            return Collections.singletonList(content);
          }
        };

    FileOutput output = (path, text) -> Assert.assertEquals(text, "TITLE: title 1\n\n");

    FeedSaverService feedSaver = new FeedSaverService(feeds, contents, output);
    feedSaver.init();
    feedSaver.run();
    feedSaver.shutdown();
  }

  @Test
  public void shouldMergeContentsToSingleFile() {
    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public List<Feed> getAll() {
            Feed feed1 =
                new FeedBuilder("https://example.test/rss1")
                    .withFilters(Collections.singleton(ContentEntryFieldType.TITLE))
                    .build();

            Feed feed2 =
                new FeedBuilder("https://example.test/rss2")
                    .withTarget("https://example.test/rss1")
                    .build();

            return Arrays.asList(feed1, feed2);
          }
        };

    ContentContainer contents =
        new DummyContentContainer() {
          @Override
          public List<Content> getAll() {
            Map<ContentEntryFieldType, String> fields1 = new HashMap<>();
            fields1.put(ContentEntryFieldType.TITLE, "title 1");
            fields1.put(ContentEntryFieldType.DESCRIPTION, "description 1");

            ContentEntry entry1 = new ContentEntry(fields1);

            Content content1 =
                new ContentBuilder("https://example.test/rss1")
                    .withEntries(Collections.singleton(entry1))
                    .build();

            Map<ContentEntryFieldType, String> fields2 = new HashMap<>();
            fields2.put(ContentEntryFieldType.TITLE, "title 2");
            fields2.put(ContentEntryFieldType.DESCRIPTION, "description 2");

            ContentEntry entry2 = new ContentEntry(fields2);

            Content content2 =
                new ContentBuilder("https://example.test/rss2")
                    .withTarget("https://example.test/rss1")
                    .withEntries(Collections.singleton(entry2))
                    .build();

            return Arrays.asList(content1, content2);
          }
        };

    FileOutput output =
        (path, text) ->
            Assert.assertEquals(
                text, "TITLE: title 1\n\nTITLE: title 2\nDESCRIPTION: description 2\n\n");

    FeedSaverService feedSaver = new FeedSaverService(feeds, contents, output);
    feedSaver.init();
    feedSaver.run();
    feedSaver.shutdown();
  }
}
