package org.ant.feedreader.service;

import org.ant.feedreader.DummyContentContainer;
import org.ant.feedreader.DummyFeedContainer;
import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.*;
import org.ant.feedreader.io.HttpInput;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class FeedLoaderServiceTest {
  @Test
  public void shouldLoadSimpleRssFeed() {
    String source = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public List<Feed> getOutdated() {
            Feed feed = new FeedBuilder(source).build();

            return Collections.singletonList(feed);
          }
        };
    ContentContainer contents =
        new DummyContentContainer() {
          @Override
          public void add(Content content) {
            Assert.assertEquals(
                "Contents should have original source", source, content.getSource());

            List<ContentEntry> entries = content.getEntries();
            Assert.assertEquals("Content should contain one entry", 1, entries.size());

            Map<ContentEntryFieldType, String> fields = entries.get(0).getFields();
            Assert.assertEquals("Example entry", fields.get(ContentEntryFieldType.TITLE));
            Assert.assertEquals(
                "Simple description", fields.get(ContentEntryFieldType.DESCRIPTION));
            Assert.assertEquals(
                "http://www.example.com/blog/post/1", fields.get(ContentEntryFieldType.LINK));

            Assert.assertArrayEquals(entries.toArray(), entries.toArray());
          }
        };

    HttpInput input =
        url ->
            new ByteArrayInputStream(
                ("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                        + "<rss version=\"2.0\">\n"
                        + "<channel>\n"
                        + " <title>RSS Title</title>\n"
                        + " <description>This is an example of an RSS feed</description>\n"
                        + " <link>http://www.example.com/main.html</link>\n"
                        + " <item>\n"
                        + "  <title>Example entry</title>\n"
                        + "  <description>Simple description</description>\n"
                        + "  <link>http://www.example.com/blog/post/1</link>\n"
                        + " </item>\n"
                        + "</channel>\n"
                        + "</rss>")
                    .getBytes(StandardCharsets.UTF_8));

    FeedLoaderService loader = new FeedLoaderService(feeds, contents, input);

    loader.init();
    loader.run();
    loader.shutdown();
  }

  @Test
  public void shouldLoadSimpleAtomFeed() {
    String source = "https://example.test/rss";

    FeedContainer feeds =
        new DummyFeedContainer() {
          @Override
          public List<Feed> getOutdated() {
            Feed feed = new FeedBuilder(source).build();

            return Collections.singletonList(feed);
          }
        };
    ContentContainer contents =
        new DummyContentContainer() {
          @Override
          public void add(Content content) {
            Assert.assertEquals(
                "Contents should have original source", source, content.getSource());

            List<ContentEntry> entries = content.getEntries();
            Assert.assertEquals("Content should contain one entry", 1, entries.size());

            Map<ContentEntryFieldType, String> fields = entries.get(0).getFields();
            Assert.assertEquals("Example entry", fields.get(ContentEntryFieldType.TITLE));
            Assert.assertEquals(
                "Simple description", fields.get(ContentEntryFieldType.DESCRIPTION));
            Assert.assertEquals(
                "http://www.example.com/blog/post/1", fields.get(ContentEntryFieldType.LINK));

            Assert.assertArrayEquals(entries.toArray(), entries.toArray());
          }
        };

    HttpInput input =
        url ->
            new ByteArrayInputStream(
                ("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                        + "<feed xmlns=\"http://www.w3.org/2005/Atom\">\n"
                        + " <title>Example Feed</title>\n"
                        + " <subtitle>A subtitle.</subtitle>\n"
                        + " <link href=\"http://example.org/feed/\" rel=\"self\" />\n"
                        + " <link href=\"http://example.org/\" />\n"
                        + " <id>urn:uuid:60a76c80-d399-11d9-b91C-0003939e0af6</id>\n"
                        + " <updated>2003-12-13T18:30:02Z</updated>\n"
                        + " <entry>\n"
                        + "  <title>Example entry</title>\n"
                        + "  <link href=\"http://www.example.com/blog/post/1\" />\n"
                        + "  <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>\n"
                        + "  <summary>Simple description</summary>\n"
                        + "  <data type=\"xhtml\">\n"
                        + "   <div xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                        + "    <p>This is the entry data.</p>\n"
                        + "   </div>\n"
                        + "  </data>\n"
                        + " </entry>\n"
                        + "</feed>")
                    .getBytes(StandardCharsets.UTF_8));

    FeedLoaderService loader = new FeedLoaderService(feeds, contents, input);

    loader.init();
    loader.run();
    loader.shutdown();
  }
}
