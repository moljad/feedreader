package org.ant.feedreader;

import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.data.Content;

import java.util.List;

public class DummyContentContainer implements ContentContainer {
  public DummyContentContainer() {}

  @Override
  public void add(Content content) {}

  @Override
  public void removeByUrl(String url) {}

  @Override
  public List<Content> getAll() {
    return null;
  }
}
