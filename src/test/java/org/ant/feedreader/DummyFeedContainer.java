package org.ant.feedreader;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class DummyFeedContainer implements FeedContainer {
  public DummyFeedContainer() {}

  @Override
  public void add(Feed feed) {}

  @Override
  public void addMany(Collection<Feed> feeds) {}

  @Override
  public Optional<Feed> get(int id) {
    return Optional.empty();
  }

  @Override
  public void remove(int id) {}

  @Override
  public List<Feed> getAll() {
    return null;
  }

  @Override
  public List<Feed> getOutdated() {
    return null;
  }
}
