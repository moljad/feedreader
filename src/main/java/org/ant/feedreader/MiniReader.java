package org.ant.feedreader;

import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.ContentContainerImpl;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.container.FeedContainerImpl;
import org.ant.feedreader.io.*;
import org.ant.feedreader.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/** Main class that manage all services and containers. */
class MiniReader {
  private final List<Service> services;

  private final ScheduledThreadPoolExecutor executor;

  MiniReader() {
    executor = new ScheduledThreadPoolExecutor(2);

    FeedContainer feeds = new FeedContainerImpl();
    ContentContainer contents = new ContentContainerImpl();

    HttpInput httpInput = new HttpInputImpl();
    FileInput fileInput = new FileInputImpl();
    FileOutput fileOutput = new FileOutputImpl();

    services = new ArrayList<>();
    services.add(new FeedPersistenceService(feeds, fileInput, fileOutput));
    services.add(new FeedLoaderService(feeds, contents, httpInput));
    services.add(new FeedSaverService(feeds, contents, fileOutput));
    services.add(new CommandHandlerService(feeds, contents));
  }

  /** Starts reader. */
  void run() {
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread(
                () -> {
                  executor.shutdown();

                  for (Service service : services) {
                    service.shutdown();
                  }
                }));

    for (Service service : services) {
      service.init();
    }

    for (Service service : services) {
      if (service.getStepDelay() > 0) {
        executor.scheduleWithFixedDelay(service, 0, service.getStepDelay(), TimeUnit.SECONDS);

      } else {
        executor.scheduleWithFixedDelay(service, 0, 100, TimeUnit.MILLISECONDS);
      }
    }
  }
}
