package org.ant.feedreader.command;

/** Interface of console command. */
public interface Command {
  /**
   * Check that command can be executed with given user input.
   *
   * @param input user input
   * @return true if command can be executed
   */
  boolean check(String input);

  /**
   * Returns command name.
   *
   * @return command name
   */
  String getName();

  /**
   * Executes command with given input.
   *
   * @param input user input
   * @return command result
   */
  String execute(String input);
}
