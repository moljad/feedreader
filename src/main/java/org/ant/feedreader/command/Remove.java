package org.ant.feedreader.command;

import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;

import java.util.Optional;
import java.util.regex.Matcher;

/** Removes existing feed. */
public class Remove extends Basic {
  private static final String NAME = "remove";
  private static final String PATTERN = "remove (\\d+)";

  private final FeedContainer feeds;
  private final ContentContainer contents;

  public Remove(FeedContainer feeds, ContentContainer contents) {
    super(NAME, PATTERN);
    this.feeds = feeds;
    this.contents = contents;
  }

  @Override
  public String execute(String input) {
    Matcher matcher = getMatcher(input);

    int id = Integer.parseInt(matcher.group(1));

    Optional<Feed> info = feeds.get(id);

    String output;
    if (info.isPresent()) {
      feeds.remove(id);

      contents.removeByUrl(info.get().getUrl());

      output = "Feed removed";
    } else {
      output = "ID not found";
    }

    return output;
  }
}
