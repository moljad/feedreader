package org.ant.feedreader.command;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.data.FeedBuilder;

import java.util.regex.Matcher;

/** Adds new feed. */
public class Add extends Basic {
  private static final String NAME = "add URL";
  private static final String PATTERN = "add (.*)";

  private final FeedContainer feeds;

  public Add(FeedContainer feeds) {
    super(NAME, PATTERN);
    this.feeds = feeds;
  }

  @Override
  public String execute(String input) {

    Matcher matcher = getMatcher(input);

    String url = matcher.group(1).trim();

    Feed feed = new FeedBuilder(url).build();

    feeds.add(feed);

    return "Url added";
  }
}
