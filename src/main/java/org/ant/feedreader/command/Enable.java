package org.ant.feedreader.command;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.data.FeedBuilder;

import java.util.Optional;
import java.util.regex.Matcher;

/** Enables existing feed. */
public class Enable extends Basic {
  private static final String NAME = "enable ID";
  private static final String PATTERN = "enable (\\d+)";

  private final FeedContainer feeds;

  public Enable(FeedContainer feeds) {
    super(NAME, PATTERN);
    this.feeds = feeds;
  }

  @Override
  public String execute(String input) {
    Matcher matcher = getMatcher(input);

    int id = Integer.parseInt(matcher.group(1));

    Optional<Feed> info = feeds.get(id);

    String output;
    if (info.isPresent()) {
      Feed newFeed = new FeedBuilder(info.get()).withEnabled(true).build();

      feeds.add(newFeed);

      output = "Feed enabled";
    } else {
      output = "ID not found";
    }

    return output;
  }
}
