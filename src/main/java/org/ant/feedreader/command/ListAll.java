package org.ant.feedreader.command;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.ContentEntryFieldType;
import org.ant.feedreader.data.Feed;

/** Shows list of all feeds with params. */
public class ListAll extends Basic {
  private static final String NAME = "list";
  private static final String PATTERN = "list";

  private final FeedContainer feeds;

  public ListAll(FeedContainer feeds) {
    super(NAME, PATTERN);
    this.feeds = feeds;
  }

  @Override
  public String execute(String input) {
    StringBuilder str = new StringBuilder();

    java.util.List<Feed> feedsAll = feeds.getAll();

    String output;
    if (!feedsAll.isEmpty()) {
      str.append("Feeds :\n");

      for (int i = 0; i < feedsAll.size(); i++) {
        Feed feed = feedsAll.get(i);
        str.append("ID: ")
            .append(i)
            .append("\nEnabled: ")
            .append(feed.isEnabled())
            .append("\nURL: ")
            .append(feed.getUrl())
            .append("\nOutput: ")
            .append(feed.getTarget())
            .append("\nMax entries: ")
            .append(feed.getCountToOutput())
            .append("\nDelay (seconds): ")
            .append(feed.getDelay())
            .append("\nStatus: ")
            .append(feed.getStatus())
            .append("\nFilters: ");

        for (ContentEntryFieldType contentEntryFieldType : feed.getFilters()) {
          str.append(contentEntryFieldType).append(' ');
        }

        if (i < feedsAll.size() - 1) {
          str.append("\n\n");
        }
      }

      output = str.toString();
    } else {
      output = "No feeds";
    }

    return output;
  }
}
