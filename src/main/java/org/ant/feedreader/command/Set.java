package org.ant.feedreader.command;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.data.FeedBuilder;

import java.util.Optional;
import java.util.regex.Matcher;

/** Sets count, output or delay settings for specific feed. */
public class Set extends Basic {
  private static final String NAME = "set ID <count|output|delay> VALUE";
  private static final String PATTERN = "set (\\d+) (count|output|delay) (.*)";

  private final FeedContainer feeds;

  public Set(FeedContainer feeds) {
    super(NAME, PATTERN);
    this.feeds = feeds;
  }

  @Override
  public String execute(String input) {
    Matcher matcher = getMatcher(input);

    int id = Integer.parseInt(matcher.group(1));
    String setting = matcher.group(2);
    String value = matcher.group(3);

    Optional<Feed> info = feeds.get(id);

    String output;
    if (info.isPresent()) {
      FeedBuilder builder = new FeedBuilder(info.get());

      switch (setting.toLowerCase()) {
        case "count":
          int count = Integer.parseInt(value);
          builder.withCountToOutput(count);
          break;
        case "output":
          builder.withTarget(value);
          break;
        case "delay":
          int delay = Integer.parseInt(value);
          builder.withDelay(delay);
          break;
      }

      feeds.add(builder.build());

      output = "Feed updated";
    } else {
      output = "ID not found";
    }

    return output;
  }
}
