package org.ant.feedreader.command;

import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.data.FeedBuilder;

import java.util.Optional;
import java.util.regex.Matcher;

/** Disables existing feed. */
public class Disable extends Basic {
  private static final String NAME = "disable ID";
  private static final String PATTERN = "disable (\\d+)";

  private final FeedContainer feeds;
  private final ContentContainer contents;

  public Disable(FeedContainer feeds, ContentContainer contents) {
    super(NAME, PATTERN);
    this.feeds = feeds;
    this.contents = contents;
  }

  @Override
  public String execute(String input) {
    Matcher matcher = getMatcher(input);

    int id = Integer.parseInt(matcher.group(1));

    Optional<Feed> info = feeds.get(id);

    String output;
    if (info.isPresent()) {
      Feed newFeed = new FeedBuilder(info.get()).withEnabled(false).build();

      feeds.add(newFeed);
      contents.removeByUrl(newFeed.getUrl());

      output = "Feed disabled";
    } else {
      output = "ID not found";
    }

    return output;
  }
}
