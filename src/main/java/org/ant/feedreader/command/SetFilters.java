package org.ant.feedreader.command;

import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.ContentEntryFieldType;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.data.FeedBuilder;
import org.ant.feedreader.data.FieldTypeDictionary;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/** Sets filters for specific feed. */
public class SetFilters extends Basic {
  private static final String NAME = "set ID filters PARAM1 [PARAM2...]";
  private static final String PATTERN = "set (\\d+) filters (.*)";

  private final FeedContainer feeds;

  public SetFilters(FeedContainer feeds) {
    super(NAME, PATTERN);
    this.feeds = feeds;
  }

  @Override
  public String execute(String input) {
    Matcher matcher = getMatcher(input);

    int id = Integer.parseInt(matcher.group(1));

    String line = matcher.group(2);

    List<String> rawParams = Arrays.asList(line.split("\\s+"));

    FieldTypeDictionary dictionary = new FieldTypeDictionary();

    List<ContentEntryFieldType> params =
        rawParams.stream()
            .filter(dictionary::check)
            .map(dictionary::toType)
            .collect(Collectors.toList());

    Optional<Feed> feed = feeds.get(id);

    String output;
    if (feed.isPresent()) {
      Feed updatedFeed = new FeedBuilder(feed.get()).withFilters(params).build();

      feeds.add(updatedFeed);

      output = "Filters set";
    } else {
      output = "Id not found";
    }

    return output;
  }
}
