package org.ant.feedreader.command;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Helper class that handles basics input parsing. */
public abstract class Basic implements Command {
  private final String name;
  private final Pattern pattern;

  Basic(String name, String pattern) {
    this.name = name;
    this.pattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
  }

  @Override
  public final boolean check(String string) {
    return pattern.matcher(string).matches();
  }

  @Override
  public final String getName() {
    return name;
  }

  /**
   * Returns Matcher for given input.
   *
   * @param input user input
   * @return matcher
   */
  Matcher getMatcher(String input) {
    Matcher matcher = pattern.matcher(input);

    //noinspection ResultOfMethodCallIgnored
    matcher.find();

    return matcher;
  }
}
