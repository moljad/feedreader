package org.ant.feedreader.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/** Default realization of {@link FileInput}. */
public class FileInputImpl implements FileInput {
  @Override
  public synchronized boolean fileExist(String path) {
    File file = new File(path);

    return file.exists();
  }

  @Override
  public synchronized String load(String path) throws IOException {
    File file = new File(path);

    FileInputStream output = new FileInputStream(file);
    byte[] data = new byte[(int) file.length()];
    output.read(data);
    output.close();

    return new String(data, StandardCharsets.UTF_8);
  }
}
