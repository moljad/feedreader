package org.ant.feedreader.io;

import java.io.IOException;

/** File input abstraction. */
public interface FileInput {
  /**
   * Checks that file exists.
   *
   * @param path path to file
   * @return true if file exist
   */
  boolean fileExist(String path);

  /**
   * Loads whole file to string.
   *
   * @param path path to file
   * @return string
   */
  String load(String path) throws IOException;
}
