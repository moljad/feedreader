package org.ant.feedreader.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/** Default realization of {@link HttpInput}. */
public class HttpInputImpl implements HttpInput {
  @Override
  public synchronized InputStream loadFeed(String url) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

    int response = conn.getResponseCode();

    // TODO Handle redirection
    /*
    if (response != 0) {
        System.out.println(response);
    }*/

    InputStream stream = conn.getErrorStream();

    if (stream == null) {
      stream = conn.getInputStream();
    }

    return stream;
  }
}
