package org.ant.feedreader.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/** Default realization of {@link FileOutput}. */
public class FileOutputImpl implements FileOutput {
  @Override
  public synchronized void write(String path, String input) throws IOException {
    File file = new File(path);

    if (!file.exists()) {
      file.createNewFile();
    }

    PrintWriter writer = new PrintWriter(file);

    writer.print(input);

    writer.close();
  }
}
