package org.ant.feedreader.io;

import java.io.IOException;

/** File output abstraction. */
public interface FileOutput {
  /**
   * Writes string to file.
   *
   * @param path path to file
   * @param input string
   */
  void write(String path, String input) throws IOException;
}
