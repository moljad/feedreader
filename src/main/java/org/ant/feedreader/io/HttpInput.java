package org.ant.feedreader.io;

import java.io.IOException;
import java.io.InputStream;

/** Abstraction of http input. */
public interface HttpInput {
  /**
   * Loads data from given url.
   *
   * @param url given url
   * @return stream
   */
  InputStream loadFeed(String url) throws IOException;
}
