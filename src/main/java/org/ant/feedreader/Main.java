package org.ant.feedreader;

/** Entry point. */
public class Main {
  public static void main(String[] args) {
    MiniReader miniReader = new MiniReader();
    miniReader.run();
  }
}
