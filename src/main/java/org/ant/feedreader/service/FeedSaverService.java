package org.ant.feedreader.service;

import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Content;
import org.ant.feedreader.data.ContentEntry;
import org.ant.feedreader.data.ContentEntryFieldType;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.io.FileOutput;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/** Service that saves loaded data to disc. */
public class FeedSaverService extends Service {
  private final FileOutput output;
  private final FeedContainer feeds;
  private final ContentContainer contents;

  public FeedSaverService(FeedContainer feeds, ContentContainer contents, FileOutput output) {
    super(10);

    this.output = output;
    this.feeds = feeds;
    this.contents = contents;
  }

  private String savePath(String path) {
    return path.replaceAll("([\\\\/:.])", "_");
  }

  @Override
  void step() {
    List<Content> allContents = contents.getAll();
    List<Feed> allFeeds = feeds.getAll();

    Set<String> targets = allContents.stream().map(Content::getTarget).collect(Collectors.toSet());

    for (String target : targets) {
      try {
        StringBuilder builder = new StringBuilder();

        List<Content> contentsGroup =
            allContents.stream()
                .filter(c -> c.getTarget().equals(target))
                .collect(Collectors.toList());

        for (Content c : contentsGroup) {
          Optional<Feed> currentFeed =
              allFeeds.stream().filter(feed -> feed.getUrl().equals(c.getSource())).findFirst();

          currentFeed.ifPresent(
              feed -> {
                for (ContentEntry entry : c.getEntries()) {
                  for (ContentEntryFieldType filter : feed.getFilters()) {
                    Map<ContentEntryFieldType, String> fields = entry.getFields();

                    if (fields.containsKey(filter)) {
                      builder.append(filter);
                      builder.append(": ");
                      builder.append(fields.get(filter));
                      builder.append("\n");
                    }
                  }
                  builder.append("\n");
                }
              });
        }

        output.write(savePath(target), builder.toString());

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
