package org.ant.feedreader.service;

import com.google.gson.Gson;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.Feed;
import org.ant.feedreader.io.FileInput;
import org.ant.feedreader.io.FileOutput;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/** Service that handles saving and loading of configuration. */
public class FeedPersistenceService extends Service {
  private static final String SETTINGS_PATH = "settings.json";

  private final FeedContainer feeds;
  private final FileInput input;
  private final FileOutput output;

  public FeedPersistenceService(FeedContainer feeds, FileInput input, FileOutput output) {
    super(1);

    this.feeds = feeds;
    this.input = input;
    this.output = output;
  }

  @Override
  public void init() {
    try {
      boolean settingsExists = input.fileExist(SETTINGS_PATH);

      if (settingsExists) {
        String json = input.load(SETTINGS_PATH);

        Gson gson = new Gson();

        Feed[] rawFeeds = gson.fromJson(json, Feed[].class);

        feeds.addMany(Arrays.asList(rawFeeds));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  void step() {}

  @Override
  public void shutdown() {
    try {
      List<Feed> allFeeds = feeds.getAll();

      Gson gson = new Gson();
      String json = gson.toJson(allFeeds);

      output.write(SETTINGS_PATH, json);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
