package org.ant.feedreader.service;

import org.ant.feedreader.command.*;
import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/** Service that handle user input. */
public class CommandHandlerService extends Service {
  private final List<Command> commands;

  public CommandHandlerService(FeedContainer feeds, ContentContainer contents) {
    super(0);

    commands = new ArrayList<>();
    commands.add(new Add(feeds));
    commands.add(new Remove(feeds, contents));
    commands.add(new ListAll(feeds));
    commands.add(new Disable(feeds, contents));
    commands.add(new Enable(feeds));
    commands.add(new Set(feeds));
    commands.add(new SetFilters(feeds));
  }

  @Override
  void step() {
    Scanner reader = new Scanner(System.in);

    boolean unknownCommand = true;

    String line = reader.nextLine();
    String output = "";

    for (Command command : commands) {
      if (command.check(line)) {
        output = command.execute(line);
        unknownCommand = false;

        break;
      }
    }

    if (unknownCommand) {
      System.out.println("Command not found. ListAll of available command:");

      for (Command command : commands) {
        System.out.println(command.getName());
      }

      System.out.println("Press ctrl+c to stop.");
    } else {
      if (!output.isEmpty()) {
        System.out.println(output);
      }
    }
  }
}
