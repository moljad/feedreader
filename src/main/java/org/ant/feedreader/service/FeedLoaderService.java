package org.ant.feedreader.service;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.ant.feedreader.container.ContentContainer;
import org.ant.feedreader.container.FeedContainer;
import org.ant.feedreader.data.*;
import org.ant.feedreader.io.HttpInput;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;

/** Service that loading feeds and stores result in {@link ContentContainer}. */
public class FeedLoaderService extends Service {
  private final HttpInput input;
  private final FeedContainer feeds;
  private final ContentContainer contents;

  public FeedLoaderService(FeedContainer feeds, ContentContainer contents, HttpInput input) {
    super(5);

    this.input = input;
    this.feeds = feeds;
    this.contents = contents;
  }

  private Content loadFeed(Feed info) throws IOException, FeedException {
    InputStream in = input.loadFeed(info.getUrl());

    SyndFeed feed = new SyndFeedInput().build(new XmlReader(in));

    List<ContentEntry> entries = new ArrayList<>();

    int count = info.getCountToOutput();
    if (count > 0) {
      for (SyndEntry entry : feed.getEntries()) {
        Map<ContentEntryFieldType, String> fields = new HashMap<>();

        String author = entry.getAuthor();
        if (author != null) {
          fields.put(ContentEntryFieldType.AUTHOR, author);
        }

        String title = entry.getTitle();
        if (title != null) {
          fields.put(ContentEntryFieldType.TITLE, title);
        }

        String link = entry.getLink();
        if (link != null) {
          fields.put(ContentEntryFieldType.LINK, link);
        }

        SyndContent description = entry.getDescription();
        if (description != null) {
          String descriptionValue = description.getValue();
          if (descriptionValue != null) {
            fields.put(ContentEntryFieldType.DESCRIPTION, descriptionValue);
          }
        }

        String uri = entry.getUri();
        if (uri != null) {
          fields.put(ContentEntryFieldType.URI, uri);
        }

        List<SyndCategory> categories = entry.getCategories();
        if (categories != null && !categories.isEmpty()) {
          StringBuilder builder = new StringBuilder();

          for (SyndCategory category : categories) {
            String name = category.getName();

            if (name != null) {
              builder.append(name);
              builder.append(' ');
            }
          }

          fields.put(ContentEntryFieldType.CATEGORY, builder.toString());
        }

        Date pubDate = entry.getPublishedDate();
        if (pubDate != null) {
          fields.put(ContentEntryFieldType.PUB_DATE, pubDate.toString());
        }

        SyndFeed syndFeed = entry.getSource();
        if (syndFeed != null) {
          String feedUrl = syndFeed.getLink();
          if (feedUrl != null) {
            fields.put(ContentEntryFieldType.SOURCE, feedUrl);
          }
        }

        entries.add(new ContentEntry(fields));

        count -= 1;
      }
    }

    ContentBuilder contentBuilder =
        new ContentBuilder(info.getUrl()).withTarget(info.getTarget()).withEntries(entries);

    return contentBuilder.build();
  }

  @Override
  void step() {
    List<Feed> feedsOutdated = feeds.getOutdated();

    for (Feed info : feedsOutdated) {
      FeedBuilder feedBuilder = new FeedBuilder(info).withLastAccess(LocalDateTime.now());

      try {
        Content content = loadFeed(info);

        feedBuilder.withStatus("loaded");

        contents.add(content);
      } catch (Exception e) {
        feedBuilder.withStatus(e.getMessage());
      }

      feeds.add(feedBuilder.build());
    }
  }
}
