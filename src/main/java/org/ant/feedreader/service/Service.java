package org.ant.feedreader.service;

/** Describes service that has 3 stages of lifetime: init, one or many steps, shutdown. */
public abstract class Service implements Runnable {
  private final long delay;

  Service(long seconds) {
    delay = seconds;
  }

  /** Method that should be called before any step. */
  public void init() {}

  /** Method that can be run many times. */
  abstract void step();

  /** Method that should be run after all steps. */
  public void shutdown() {}

  /**
   * Returns time (in seconds) between steps.
   *
   * @return time
   */
  public final long getStepDelay() {
    return delay;
  }

  @Override
  public final void run() {
    step();
  }
}
