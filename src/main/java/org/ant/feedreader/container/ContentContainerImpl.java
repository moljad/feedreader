package org.ant.feedreader.container;

import org.ant.feedreader.data.Content;

import java.util.ArrayList;
import java.util.List;

/** Default implementation of {@link ContentContainer}. */
public class ContentContainerImpl implements ContentContainer {
  private final List<Content> contents;

  public ContentContainerImpl() {
    contents = new ArrayList<>();
  }

  @Override
  public synchronized void add(Content content) {
    for (int i = 0; i < contents.size(); i++) {
      Content f = contents.get(i);

      if (f.getSource().equals(content.getSource())) {
        contents.set(i, content);
        return;
      }
    }

    contents.add(content);
  }

  @Override
  public synchronized void removeByUrl(String url) {
    contents.removeIf(c -> c.getSource().equals(url));
  }

  @Override
  public synchronized List<Content> getAll() {
    return new ArrayList<>(contents);
  }
}
