package org.ant.feedreader.container;

import org.ant.feedreader.data.Content;

import java.util.List;

/** Interface for container that contains {@link Content}s. */
public interface ContentContainer {
  /**
   * Add new data.
   *
   * @param content new data
   */
  void add(Content content);

  /**
   * Removes all contents with same source url.
   *
   * @param url source url
   */
  void removeByUrl(String url);

  /**
   * Returns all contents.
   *
   * @return List of contents.
   */
  List<Content> getAll();
}
