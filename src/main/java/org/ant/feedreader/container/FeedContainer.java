package org.ant.feedreader.container;

import org.ant.feedreader.data.Feed;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/** Interface for container that contains {@link Feed}s. */
public interface FeedContainer {
  /**
   * Adds new {@link Feed}.
   *
   * @param feed new feed
   */
  void add(Feed feed);

  /**
   * Adds many {@link Feed}s.
   *
   * @param feeds list of new feeds
   */
  void addMany(Collection<Feed> feeds);

  /**
   * Returns {@link Feed} by id.
   *
   * @param id feed id
   * @return feed
   */
  Optional<Feed> get(int id);

  /**
   * Removes {@link Feed} by id.
   *
   * @param id feed id
   */
  void remove(int id);

  /**
   * Returns all {@link Feed}s.
   *
   * @return list of all feeds
   */
  List<Feed> getAll();

  /**
   * Returns all outdated {@link Feed}s
   *
   * @return list of all feeds
   */
  List<Feed> getOutdated();
}
