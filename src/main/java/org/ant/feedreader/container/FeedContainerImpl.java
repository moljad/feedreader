package org.ant.feedreader.container;

import org.ant.feedreader.data.Feed;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;

/** Default implementation of {@link FeedContainer}. */
public class FeedContainerImpl implements FeedContainer {
  private final List<Feed> feeds;

  public FeedContainerImpl() {
    feeds = new ArrayList<>();
  }

  @Override
  public synchronized void add(Feed feed) {
    for (int i = 0; i < feeds.size(); i++) {
      Feed f = feeds.get(i);

      if (f.getUrl().equals(feed.getUrl())) {
        feeds.set(i, feed);
        return;
      }
    }

    feeds.add(feed);
  }

  @Override
  public synchronized void addMany(Collection<Feed> feeds) {
    this.feeds.addAll(feeds);
  }

  @Override
  public synchronized Optional<Feed> get(int id) {
    if (id >= 0 && id < feeds.size()) {
      return Optional.of(feeds.get(id));
    }

    return Optional.empty();
  }

  @Override
  public synchronized void remove(int id) {
    if (id >= 0 && id < feeds.size()) {
      feeds.remove(id);
    }
  }

  @Override
  public synchronized List<Feed> getAll() {
    return new ArrayList<>(feeds);
  }

  @Override
  public synchronized List<Feed> getOutdated() {
    return feeds.stream()
        .filter(
            (x) -> {
              LocalDateTime now = LocalDateTime.now();

              boolean isNotUpdated = SECONDS.between(x.getLastAccess(), now) > x.getDelay();
              boolean isEnabled = x.isEnabled();

              return isEnabled && isNotUpdated;
            })
        .collect(Collectors.toList());
  }
}
