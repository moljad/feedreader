package org.ant.feedreader.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class that describe feed.
 *
 * <p>Immutable. {@link FeedBuilder} can be used for construction and mutation.
 */
public final class Feed {
  /** Feed url. */
  private final String url;

  /** Target file path. */
  private final String target;

  /** Max count of entities that can be written to file. */
  private final int countToOutput;

  /** List of fields that can be shown be written to file. */
  private final List<ContentEntryFieldType> filters;

  /** Status of feed: active or not. */
  private final boolean enabled;

  /** Time of last access. */
  private final LocalDateTime lastAccess;
  /** Delay between loadings in seconds. */
  private final long delay;

  /** Last saved status. Contain result of last operation. For example error code. */
  private final String status;

  /**
   * Default constructor.
   *
   * @param url feed url
   * @param target target path
   * @param countToOutput count
   * @param filters list of filters
   * @param enabled status
   * @param lastAccess last access time
   * @param delay delay between loads in seconds
   * @param status last saved status
   */
  public Feed(
      String url,
      String target,
      int countToOutput,
      Collection<ContentEntryFieldType> filters,
      boolean enabled,
      LocalDateTime lastAccess,
      long delay,
      String status) {
    this.url = url;
    this.target = target;
    this.countToOutput = countToOutput;
    this.filters = new ArrayList<>(filters);
    this.enabled = enabled;
    this.lastAccess = lastAccess;
    this.delay = delay;
    this.status = status;
  }

  /**
   * Returns url.
   *
   * @return url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Returns target path.
   *
   * @return path
   */
  public String getTarget() {
    return target;
  }

  /**
   * Returns max count of entities that can be written to file.
   *
   * @return count
   */
  public int getCountToOutput() {
    return countToOutput;
  }

  /**
   * Return filters.
   *
   * @return list of filters
   */
  public List<ContentEntryFieldType> getFilters() {
    return new ArrayList<>(filters);
  }

  /**
   * Returns status.
   *
   * @return status
   */
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * Returns last access time.
   *
   * @return time
   */
  public LocalDateTime getLastAccess() {
    return lastAccess;
  }

  /**
   * Returns delay between loads.
   *
   * @return delay
   */
  public long getDelay() {
    return delay;
  }

  /**
   * Retursn status.
   *
   * @return status
   */
  public String getStatus() {
    return status;
  }
}
