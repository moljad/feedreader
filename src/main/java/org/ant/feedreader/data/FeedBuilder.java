package org.ant.feedreader.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** Helper class that can be used to construct new {@link Feed}. */
public class FeedBuilder {
  private String url;
  private String target;
  private boolean enabled;
  private LocalDateTime lastAccess;
  private long delay;
  private int countToOutput;
  private String status;

  private Collection<ContentEntryFieldType> filters;

  /**
   * Default constructor for new {@link Feed}.
   *
   * @param url url
   */
  public FeedBuilder(String url) {
    this.url = url;
    this.target = url;
    this.enabled = true;
    this.lastAccess = LocalDateTime.now();
    this.countToOutput = Integer.MAX_VALUE;
    this.delay = 30;
    this.status = "";

    List<ContentEntryFieldType> filters = new ArrayList<>();
    filters.add(ContentEntryFieldType.TITLE);
    filters.add(ContentEntryFieldType.DESCRIPTION);
    filters.add(ContentEntryFieldType.SOURCE);

    this.filters = filters;
  }

  /**
   * Copy constructor for updating {@link Feed}.
   *
   * @param feed existing feed
   */
  public FeedBuilder(Feed feed) {
    url = feed.getUrl();
    target = feed.getTarget();
    enabled = feed.isEnabled();
    lastAccess = feed.getLastAccess();
    delay = feed.getDelay();
    countToOutput = feed.getCountToOutput();
    status = feed.getStatus();
    filters = feed.getFilters();
  }

  /**
   * Returns constructed {@link Feed}.
   *
   * @return feed
   */
  public Feed build() {
    return new Feed(url, target, countToOutput, filters, enabled, lastAccess, delay, status);
  }

  public FeedBuilder withUrl(String url) {
    this.url = url;
    return this;
  }

  /**
   * Changes target path.
   *
   * @param target new path
   * @return builder
   */
  public FeedBuilder withTarget(String target) {
    this.target = target;
    return this;
  }

  /**
   * Changes status of the feed.
   *
   * @param enabled new status
   * @return builder
   */
  public FeedBuilder withEnabled(boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  /**
   * Changes last accessed time.
   *
   * @param lastAccess new time
   * @return builder
   */
  public FeedBuilder withLastAccess(LocalDateTime lastAccess) {
    this.lastAccess = lastAccess;
    return this;
  }

  /**
   * Changes delay.
   *
   * @param delay new delay
   * @return builder
   */
  public FeedBuilder withDelay(long delay) {
    this.delay = delay;
    return this;
  }

  /**
   * Changes count of the feed.
   *
   * @param countToOutput new count
   * @return builder
   */
  public FeedBuilder withCountToOutput(int countToOutput) {
    this.countToOutput = countToOutput;
    return this;
  }

  /**
   * Changes filters.
   *
   * @param filters new filters
   * @return builder
   */
  public FeedBuilder withFilters(Collection<ContentEntryFieldType> filters) {
    this.filters = filters;
    return this;
  }

  /**
   * Changes recent status of the feed.
   *
   * @param status new status
   * @return builder
   */
  public FeedBuilder withStatus(String status) {
    this.status = status;
    return this;
  }
}
