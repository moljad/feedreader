package org.ant.feedreader.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Content of specific {@link Feed}.
 *
 * <p>Immutable. {@link ContentBuilder} can be used for construction and mutation.
 */
public final class Content {
  /** Url of {@link Feed}. */
  private final String source;

  /** Path to target file. */
  private final String target;

  /** Collection of {@link ContentEntry}s. */
  private final List<ContentEntry> entries;

  public Content(String source, String target, Collection<ContentEntry> entries) {
    this.source = source;
    this.target = target;
    this.entries = new ArrayList<>(entries);
  }

  /**
   * Returns url of {@link Feed}
   *
   * @return url
   */
  public String getSource() {
    return source;
  }

  /**
   * Return path to target file.
   *
   * @return path
   */
  public String getTarget() {
    return target;
  }

  /**
   * Returns all {@link ContentEntry}s.
   *
   * @return list of ContentEntry
   */
  public List<ContentEntry> getEntries() {
    return new ArrayList<>(entries);
  }
}
