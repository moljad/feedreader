package org.ant.feedreader.data;

import java.util.HashMap;
import java.util.Map;

public class FieldTypeDictionary {
  private final Map<String, ContentEntryFieldType> to;
  private final Map<ContentEntryFieldType, String> from;

  public FieldTypeDictionary() {
    to = new HashMap<>();
    to.put("title", ContentEntryFieldType.TITLE);
    to.put("description", ContentEntryFieldType.DESCRIPTION);
    to.put("link", ContentEntryFieldType.LINK);
    to.put("source", ContentEntryFieldType.SOURCE);
    to.put("author", ContentEntryFieldType.AUTHOR);
    to.put("category", ContentEntryFieldType.CATEGORY);
    to.put("pubdate", ContentEntryFieldType.PUB_DATE);
    to.put("uri", ContentEntryFieldType.URI);

    from = new HashMap<>();
    from.put(ContentEntryFieldType.TITLE, "title");
    from.put(ContentEntryFieldType.DESCRIPTION, "description");
    from.put(ContentEntryFieldType.LINK, "link");
    from.put(ContentEntryFieldType.SOURCE, "source");
    from.put(ContentEntryFieldType.AUTHOR, "author");
    from.put(ContentEntryFieldType.CATEGORY, "category");
    from.put(ContentEntryFieldType.PUB_DATE, "pubdate");
    from.put(ContentEntryFieldType.URI, "uri");
  }

  public FieldTypeDictionary(
      Map<String, ContentEntryFieldType> to, Map<ContentEntryFieldType, String> from) {
    this.to = to;
    this.from = from;
  }

  public ContentEntryFieldType toType(String string) {
    return to.get(string.toLowerCase());
  }

  public String fromType(ContentEntryFieldType type) {
    return from.get(type);
  }

  public boolean check(String string) {
    return to.containsKey(string);
  }
}
