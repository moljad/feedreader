package org.ant.feedreader.data;

/** Enum of available fields types (RSS Items). */
public enum ContentEntryFieldType {
  AUTHOR,
  URI,
  CATEGORY,
  LINK,
  DESCRIPTION,
  PUB_DATE,
  TITLE,
  SOURCE,
}
