package org.ant.feedreader.data;

import java.util.HashMap;
import java.util.Map;

/** Content entry that contains fields. */
public final class ContentEntry {
  /** Collection of fields. */
  private final Map<ContentEntryFieldType, String> fields;

  /** Constructor for empty entries. */
  public ContentEntry() {
    fields = new HashMap<>();
  }

  /**
   * Constructor for complete entries.
   *
   * @param fields fields
   */
  public ContentEntry(Map<ContentEntryFieldType, String> fields) {
    this.fields = fields;
  }

  public Map<ContentEntryFieldType, String> getFields() {
    return new HashMap<>(fields);
  }
}
