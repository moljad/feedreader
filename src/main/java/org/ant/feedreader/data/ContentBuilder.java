package org.ant.feedreader.data;

import java.util.ArrayList;
import java.util.Collection;

/** Helper class that can be used to construct new {@link Content}. */
public class ContentBuilder {
  private String source;
  private String target;

  private Collection<ContentEntry> entries;

  /**
   * Default constructor for new {@link Content}.
   *
   * @param source url of source feed
   */
  public ContentBuilder(String source) {
    this.source = source;
    this.target = source;

    this.entries = new ArrayList<>();
  }

  /**
   * Copy constructor for updating {@link Content}.
   *
   * @param content existing data
   */
  public ContentBuilder(Content content) {
    this.source = content.getSource();
    this.target = content.getTarget();

    this.entries = content.getEntries();
  }

  /**
   * Returns constructed {@link Content}.
   *
   * @return data
   */
  public Content build() {
    return new Content(source, target, entries);
  }

  /**
   * Changes url of source feed.
   *
   * @param source new url
   * @return builder
   */
  public ContentBuilder withSource(String source) {
    this.source = source;
    return this;
  }

  /**
   * Changes target path of data.
   *
   * @param target new path
   * @return builder
   */
  public ContentBuilder withTarget(String target) {
    this.target = target;
    return this;
  }

  /**
   * Changes data entries.
   *
   * @param entries new entries
   * @return builder
   */
  public ContentBuilder withEntries(Collection<ContentEntry> entries) {
    this.entries = entries;
    return this;
  }
}
